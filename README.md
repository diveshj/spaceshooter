# SpaceShooter

SpaceShooter made using [Simple and Fast Multimedia Library (SFML)](https://www.sfml-dev.org/).

```
WASD to move.
Spacebar to shoot.
P to pause.
R to restart (once dead).
```
Has a data driven editor using JSON and Visual elements using WPF repo  [SpaceShooter WPF Editor](https://gitlab.com/diveshj/prog56693-project-1-settings).

Telemetry is also generated during gameplay and stored in a SQL Database. This WPF tool allows you to open and browse that data [SpaceShooter SQL WPF](https://gitlab.com/diveshj/prog56693-project-1-sql-wpf).

![Gameplay](https://gitlab.com/diveshj/project-images-dump/-/raw/main/SpaceShooter/play.jpg)
![Gameplay](https://gitlab.com/diveshj/project-images-dump/-/raw/main/SpaceShooter/play2.jpg)
![Game Over](https://gitlab.com/diveshj/project-images-dump/-/raw/main/SpaceShooter/dead.jpg)
