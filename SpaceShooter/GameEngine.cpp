#include "GameEngine.h"


void GameEngine::Initialize()
{
	windowattr = new windowAttr();
	windowattr->getWindowAttrJSON();
	player = new Player();
	player->initializeJSON();
	enemyController = new EnemyController();
	asteroidController = new AsteroidController();
	asteroidController->initializeJSON();
	backgroundController = new BackgroundController();
	backgroundController->initializeJSON();
	uiController = new UIController();
	uiController->initializeJSON();
}
void GameEngine::GameLoop()
{
	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(windowattr->width, windowattr->height), windowattr->title);
	window->setFramerateLimit(60);

	player->setWindow(window);
	enemyController->setWindow(window);
	asteroidController->setWindow(window);
	backgroundController->setWindow(window);
	backgroundController->setWindow(window);
	uiController->setWindow(window);

	//Time
	std::chrono::time_point<std::chrono::system_clock> _time;
	std::chrono::duration<float> deltaTime(0);
	std::chrono::duration<float> totalTime(0);

	while (window != nullptr )
	{

		_time = std::chrono::system_clock::now();

		window->clear();
		if (window != NULL)
		{
			sf::Event event;
			while (window != nullptr && window->pollEvent(event))
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				{
					sf::Event::Closed;
					uiController->writeHS();
					window->close();
					delete window;
					window = nullptr;
					break;
				}

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					if (player->getLives() <= 0)
					{
						player->UpdateDatabase(totalTime.count());
						totalTime -= totalTime;
					uiController->writeHS();
					enemyController->Reset();
					asteroidController->Reset();
					player->Reset();
					}
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tilde))
				{
					//TODO
					//player.JSONWriteToFile();
					//enemyController.JSONWriteToFile();
					//asteroidController.JSONWriteToFile();
				}
				switch (event.type)
				{
				case sf::Event::Closed:
					uiController->writeHS();
					window->close();
					delete window;
					window = nullptr;
					break;
				}
			}

			if (window != nullptr )
			{
				if (player->getLives() > 0)
				{
					uiController->initalizeText();
				backgroundController->setBackground();
				backgroundController->Update((deltaTime.count()));
				player->Movement(event, deltaTime.count());
				player->BoundsChecks();
				player->ProjMovement(deltaTime.count());

				enemyController->EnemyTimer(deltaTime.count());
				asteroidController->AsteroidTimer(deltaTime.count());
				enemyController->EnemyUpdates(deltaTime.count());
				asteroidController->AsteroidUpdate(deltaTime.count());

				std::vector <Enemy>& vecEnemy = enemyController->GetEnemyT1Vec();
				player->CollisionChecks(vecEnemy);
				std::vector <Asteroid*>& vecAsteroid = asteroidController->getAsteroidsVec();
				player->CollisionChecksAsteroid(vecAsteroid);
				uiController->UIsetLives(player->getLives());
				uiController->UIsetScore(player->getScore());
				}
				else
				{
					uiController->DisplayGameOver();
				}

				player->Draw();
				asteroidController->AsteroidDraw();
				enemyController->EnemyDraw();
				uiController->DisplayText();
				uiController->DisplayLives();

				window->display();
			}
		}
		deltaTime = std::chrono::system_clock::now() - _time;
		totalTime += deltaTime;
	}
	player->UpdateDatabase(totalTime.count());
	std::cout << " You played game for " << totalTime.count() << " seconds" << std::endl;
}

int main()
{
	GameEngine::Instance().Initialize();
	GameEngine::Instance().GameLoop();
	return 0;
}