#pragma once
#include "StandardIncludes.h"
#include "Asteroid.h"
#include <iostream>
class AsteroidController
{
public:
	AsteroidController();
	~AsteroidController();
	void AsteroidTimer(float _dt);
	int SelectText();
	int SelectSpawnDir();
	void AsteroidDraw();
	void AsteroidUpdate(float _dt);
	std::vector<Asteroid*>& getAsteroidsVec();
	void Reset();
	void setWindow(sf::RenderWindow* _window);
	void initializeJSON();
	void getCount();



private:
	std::vector<Asteroid*> asteroids;

	sf::Texture* meteorBig = new sf::Texture;
	sf::Texture* meteorSmall = new sf::Texture;

	sf::RenderWindow*  window;

	float timer = 0;
	int count = 0;

};

