#pragma once
#include "StandardIncludes.h"
class Projectile
{
public:
	
	Projectile(sf::Texture* texture, sf::Vector2f pos);
	~Projectile();
	sf::Sprite GetSprite();

	sf::Sprite projectile;

};

