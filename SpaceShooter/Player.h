#pragma once
#include "StandardIncludes.h"
#include "Projectile.h"
#include "Enemy.h"
#include "Asteroid.h"
#include "AsteroidController.h"
#include "DBManager.h"

class Player
{
public:
	Player();
	~Player();
	void initializeJSON();
	void setWindow(sf::RenderWindow* _window);
	void BoundsChecks();
	void Draw();
	void Movement(sf::Event event, float _dt);
	void ActualShoot();
	void ProjMovement(float _dt);
	void ProjDraw();
	void CollisionChecks(std::vector <Enemy>& _Enemies);
	void CollisionChecksAsteroid(std::vector <Asteroid*>& _Asteroids);
	int getLives();
	int getScore();
	void Reset();
	void JSONWriteToFile();
	void UpdateDatabase(float _totalTime);

	//members
private:
	sf::Sprite ship;
	sf::Texture* shipTexture = new sf::Texture();

	sf::Texture* projTexture = new sf::Texture();

	sf::Sprite initProj;
	sf::Texture* initProjTexture = new sf::Texture();

	std::vector<Projectile> pLaser;
	int HP;
	int shootTimer;
	sf::RenderWindow* window;

	int damage;
	int score;

	int collCounter;
	int deathCounter;
	int pDestroyedEnemies;
	int pDestroyedAsteroid;

	DBManager* dbManager = new DBManager();

};

