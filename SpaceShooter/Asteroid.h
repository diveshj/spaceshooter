#pragma once
#include "StandardIncludes.h"
class Asteroid
{
public:
	Asteroid();
		~Asteroid();
		void setTexture(sf::Texture* _texture);
		void setSpawnLoc(sf::RenderWindow* _window, int dir);
		void setMoveDir(sf::Vector2f _dir);
		void Update(sf::RenderWindow* _window, float _dt);
		sf::Sprite getSprite();
		int getScoreVal();
private:
	sf::Sprite asteroid;
	sf::Vector2f moveDir;
	int points = 50;
};

