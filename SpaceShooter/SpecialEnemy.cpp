#include "SpecialEnemy.h"
#include <iostream>


SpecialEnemy::SpecialEnemy()
{
if (!this->shipTexture->loadFromFile("Enemies/enemyUFO.png"))
{

	std::cout << "Unable to load Texture" << std::endl;
}
this->scoreVal = 100;
this->HP = 5;
this->shootTimer = 0;
this->ship.setTexture(*shipTexture);
this->initProjTexture->loadFromFile("Enemies/laserRedShot.png");
this->initProj.setTexture(*initProjTexture);
this->projTexture->loadFromFile("Enemies/laserRed.png");

moveDir.x = 0;
moveDir.y = 0;
}

SpecialEnemy::~SpecialEnemy()
{
}

//Sets the spawn location on the X axis somewhere
void SpecialEnemy::SetSpawnLocation(float _x)
{
	this->ship.setPosition(_x, 0.0f);
}

//Sets the initial direction movement based on the bool provided. Moves left if spawned on right, right if spawned on left
void SpecialEnemy::SetInitMovement(bool dir, float _dt)
{
	if (dir == false)
	{
		moveDir.x = -100.0f;
		moveDir.y = 120.0f;
		this->ship.move(moveDir.x * _dt, moveDir.y * _dt);
	}
	else
	{
		moveDir.x = 100.0f;
		moveDir.y = 120.0f;
		this->ship.move(moveDir.x * _dt, moveDir.y * _dt);
	}
}

//All updates on Enemy, shooting, general updates
void SpecialEnemy::Update(sf::RenderWindow* _window, float _dt)
{
	if (shootTimer > 24)
	{
		sf::Vector2f laserVec = ship.getPosition();
		laserVec.x = laserVec.x + (shipTexture->getSize().x * 0.5);
		laserVec.y = laserVec.y + (shipTexture->getSize().y);
		this->pLaser.push_back(Projectile(projTexture, laserVec));

		sf::Vector2f initProjVec = ship.getPosition();
		initProjVec.x = initProjVec.x + (shipTexture->getSize().x * 0.5);
		initProjVec.y = initProjVec.y + (shipTexture->getSize().y);
		Projectile* temp = new Projectile(initProjTexture, initProjVec);
		_window->draw(temp->projectile);

		shootTimer = 0;
	}
	shootTimer++;
	ProjDraw(_window);
	ProjMovement(_window, _dt);

	this->ship.move(moveDir.x * _dt, moveDir.y * _dt);

}

sf::Sprite SpecialEnemy::getSprite()
{
	return ship;
}

//Draws the laser
void SpecialEnemy::ProjDraw(sf::RenderWindow* _window)
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		_window->draw(pLaser[i].projectile);
	}
}


//laser flowing through the world and its deletion
void SpecialEnemy::ProjMovement(sf::RenderWindow* _window, float _dt)
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		this->pLaser[i].projectile.move(0.0f, 350.0f * _dt);

		if (pLaser[i].projectile.getPosition().y > _window->getSize().y)
		{
			this->pLaser.erase(pLaser.begin() + i);
		}
	}
}

int SpecialEnemy::GetHP()
{
	return HP;
}

int SpecialEnemy::GetScoreVal()
{
	return scoreVal;
}

void SpecialEnemy::TakeDamage()
{
	this->HP--;
}