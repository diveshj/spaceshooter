#pragma once
#define SINGLETON(_Singleton_) \
public: \
	inline static _Singleton_& Instance() \
	{\
	static _Singleton_ instance; \
	return instance;\
	}\
\
private: \
	inline explicit _Singleton_() = default;\
	inline ~_Singleton_() = default; \
	inline explicit _Singleton_(_Singleton_ const&) = delete; \
	inline _Singleton_& operator = (_Singleton_ const&) = delete;
