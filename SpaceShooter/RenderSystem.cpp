#include "RenderSystem.h"
#include <iostream>

RenderSystem::RenderSystem() :
	name(""),
	width(-1),
	height(-1),
	fullscreen(false)
{
	std::cout << "\tRenderSystem Created" << std::endl;
}

RenderSystem::~RenderSystem()
{
	std::cout << "\tRenderSystem Destroyed" << std::endl;
}

void RenderSystem::Initialize()
{
	std::cout << "\tRenderSystem Initialized" << std::endl;
}

void RenderSystem::Load(json::JSON& _jsonNode)
{
	std::cout << "\tRenderSystem Loaded" << std::endl;

	if (!_jsonNode.hasKey("RenderSystem")) throw std::exception("RenderSystem element is missing");
	json::JSON& renderSystemNode = _jsonNode["RenderSystem"];

	if (!renderSystemNode.hasKey("Name")) throw std::exception("Name element is missing");
	name = renderSystemNode["Name"].ToString();

	if (!renderSystemNode.hasKey("width")) throw std::exception("width element is missing");
	width = renderSystemNode["width"].ToInt();

	if (!renderSystemNode.hasKey("height")) throw std::exception("height element is missing");
	height = renderSystemNode["height"].ToInt();

	if (!renderSystemNode.hasKey("fullscreen")) throw std::exception("fullscreen element is missing");
	fullscreen = renderSystemNode["fullscreen"].ToBool();
}

void RenderSystem::Update()
{
	std::cout << "\tRenderSystem Update" << std::endl;
}

void RenderSystem::Display()
{
	std::cout << "\tRender System" << std::endl;
	std::cout << "\t\tName: " << name.c_str() << std::endl;
	std::cout << "\t\tWidth: " << width << std::endl;
	std::cout << "\t\tHeight: " << height << std::endl;
	std::cout << "\t\tFullScreen: " << fullscreen << std::endl;

}
