#include "Player.h"


Player::Player()
{
	this->shootTimer = 0;
	this->score = 0;

	this->collCounter = 0;
	this->deathCounter = 0;
	this->pDestroyedEnemies = 0;
	this->pDestroyedAsteroid = 0;
}

Player::~Player()
{
}
//Loads up all the player data from JSON
void Player::initializeJSON()
{
	std::ifstream inputStream("./Assets/JSON/Player.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON playerfileJSON = json::JSON::Load(str);
	
		if (!playerfileJSON.hasKey("HP")) throw std::exception("HP Missing");
		this->HP = playerfileJSON["HP"].ToInt();

		if (!playerfileJSON.hasKey("shipTexture")) throw std::exception("shipTexture Missing");
		{
			std::string loc = playerfileJSON["shipTexture"].ToString();
			if (!this->shipTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!playerfileJSON.hasKey("initProjTexture")) throw std::exception("initProjTexture Missing");
		{
			std::string loc = playerfileJSON["initProjTexture"].ToString();
			if (!this->initProjTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!playerfileJSON.hasKey("projTexture")) throw std::exception("projTexture Missing");
		{
			std::string loc = playerfileJSON["projTexture"].ToString();
			if (!this->projTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}
		if (!playerfileJSON.hasKey("damage")) throw std::exception("damage Missing");
		this->damage = playerfileJSON["damage"].ToInt();

	this->ship.setTexture(*shipTexture);

	this->initProj.setTexture(*initProjTexture);


}

void Player::setWindow(sf::RenderWindow* _window)
{
	window = _window;
	this->ship.setPosition(window->getSize().x * 0.5, window->getSize().y * 0.8);
}

//Player bound checks for the screen
void Player::BoundsChecks()
{

	if (this->ship.getPosition().x <= 0)
		this->ship.setPosition(0.0f, this->ship.getPosition().y);
	if (this->ship.getPosition().x >= window->getSize().x - this->ship.getGlobalBounds().width)
		this->ship.setPosition(window->getSize().x - this->ship.getGlobalBounds().width, this->ship.getPosition().y);
	if (this->ship.getPosition().y <= 0)
		this->ship.setPosition(this->ship.getPosition().x, 0.f);
	if (this->ship.getPosition().y >= window->getSize().y - this->ship.getGlobalBounds().height)
		this->ship.setPosition(this->ship.getPosition().x, window->getSize().y - this->ship.getGlobalBounds().height);
}

//Function call for Player as well as Projectiles to be drawn
void Player::Draw()
{
	window->draw(ship);
	ProjDraw();
}


//Takes player inputs and parses it
void Player::Movement(sf::Event event, float _dt)
{
	shootTimer++;
	if (window->hasFocus() == true)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			ship.move(0.0f * _dt, -220.0f * _dt);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			ship.move(-220.0f * _dt, 0.0f * _dt);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			ship.move(0.0f * _dt, 220.0f * _dt);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			ship.move(220.0f * _dt, 0.0f * _dt);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && shootTimer >= 8)
		{
			this->ActualShoot();
			shootTimer = 0;
		}
	}
}

//Creates and instantiates the projectile - Note the initial sprite for firing the laser is drawn here while the vector for laser passing through the screen is done separately
void Player::ActualShoot()
{
	sf::Vector2f laserVec = ship.getPosition();
	laserVec.x = laserVec.x + (shipTexture->getSize().x * 0.5);
	this->pLaser.push_back(Projectile(projTexture, laserVec));

	sf::Vector2f initProjVec = ship.getPosition();
	initProjVec.x = initProjVec.x + (shipTexture->getSize().x * 0.5);
	Projectile* temp = new Projectile(initProjTexture, initProjVec);
	window->draw(temp->projectile);
}


//This function handles the projectile after it has been created till its destroyed when it moves out of the screen
void Player::ProjMovement(float _dt)
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		this->pLaser[i].projectile.move(0.0f * _dt, -350.0f * _dt);

		if (pLaser[i].projectile.getPosition().y < 0)
		{
			pLaser.erase(pLaser.begin() + i);
		}
	}
}

//This draws the projectiles on each frame
void Player::ProjDraw()
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		window->draw(pLaser[i].projectile);
	}
}

//All the collision checks in the world! Destroy when any of them intersect. There are SO MANY LOOPS HERE THAT ITS SUPER SLOW
//This one is for Player Ship and Enemy Ship
void Player::CollisionChecks(std::vector<Enemy>& _Enemies)
{
	for (int i = 0; i < (int)_Enemies.size(); i++)
	{
		if (this->ship.getGlobalBounds().intersects(_Enemies[i].getSprite().getGlobalBounds()))
		{
			collCounter++;
			deathCounter++;
			pDestroyedEnemies++;
			this->HP--;
			_Enemies.erase(_Enemies.begin() + i);
		}
	}

	//This one is for Player Ship and Enemy laser
	for (int i = 0; i < (int)_Enemies.size(); i++)
	{
		std::vector<Projectile>& enemyProj = _Enemies[i].GetProjectiles();
		for (int k = 0; k < (int)enemyProj.size(); k++)
		{
			if (this->ship.getGlobalBounds().intersects(enemyProj[k].projectile.getGlobalBounds()))
			{
				deathCounter++;
				HP = HP - _Enemies[i].getDamage();
				enemyProj.erase(enemyProj.begin() + k);
				break;
			}
		}

	}
	//This one is for player laser and enemy ship
	for (int i = 0; i < (int)pLaser.size(); i++)
	{

		for (int j = 0; j < (int)_Enemies.size(); j++)
		{
			if (pLaser[i].projectile.getGlobalBounds().intersects(_Enemies[j].getSprite().getGlobalBounds()))
			{
				if (_Enemies[j].GetHP() <= 1)
				{
					pDestroyedEnemies++;
					score += _Enemies[j].GetScoreVal();
					_Enemies.erase(_Enemies.begin() + j);
				}
				else
				{
					_Enemies[j].TakeDamage(damage);
				}

				pLaser.erase(pLaser.begin() + i);
				break;
			}
		}
	}
	//This is for Player Laser and Enemy Laser
	for (int i = 0; i < (int)_Enemies.size(); i++)
	{
		std::vector<Projectile>& enemyProj = _Enemies[i].GetProjectiles();
		for (int j = 0; j < (int)enemyProj.size(); j++)
		{

			for (int k = 0; k < (int)pLaser.size(); k++)
			{
				if (pLaser[k].projectile.getGlobalBounds().intersects(enemyProj[j].projectile.getGlobalBounds()))
				{
					pLaser.erase(pLaser.begin() + k);
					enemyProj.erase(enemyProj.begin() + j);
					break;
				}
			}

		}
	}
}

//Collision checks for asteroid 
//This one is for Player Ship and Asteroid
void Player::CollisionChecksAsteroid(std::vector<Asteroid*>& _Asteroids)
{
	for (int i = 0; i < (int)_Asteroids.size(); i++)
	{
		if (this->ship.getGlobalBounds().intersects(_Asteroids[i]->getSprite().getGlobalBounds()))
		{
			pDestroyedAsteroid++;
			collCounter++;
			deathCounter++;
			this->HP--;
			_Asteroids.erase(_Asteroids.begin() + i);
		}
	}

	//This is for Player Projectile and Asteroid
	for (int i = 0; i < (int)pLaser.size(); i++)
	{

		for (int j = 0; j < (int)_Asteroids.size(); j++)
		{
			if (pLaser[i].projectile.getGlobalBounds().intersects(_Asteroids[j]->getSprite().getGlobalBounds()))
			{
				pDestroyedAsteroid++;
				score += _Asteroids[j]->getScoreVal();
				_Asteroids.erase(_Asteroids.begin() + j);
				pLaser.erase(pLaser.begin() + i);
				break;
			}
		}
	}
}

//Returns the number of lives left for the player
int Player::getLives()
{
	return HP;
}

//Returns the Player Score
int Player::getScore()
{
	return score;
}

//Reset the game here
void Player::Reset()
{

	this->ship.setPosition(window->getSize().x * 0.5, window->getSize().y * 0.8);
	score = 0;
	collCounter = 0;
	deathCounter = 0;
	pDestroyedEnemies = 0;
	pDestroyedAsteroid = 0;
	pLaser.clear();

	std::ifstream inputStream("./Assets/JSON/Player.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON playerfileJSON = json::JSON::Load(str);
		if (!playerfileJSON.hasKey("HP")) throw std::exception("HP Missing");
		this->HP = playerfileJSON["HP"].ToInt();

}

void Player::JSONWriteToFile()
{

	//Would be implementing JSON WRite to file here if I had more time
	json::JSON document;
	json::JSON subObject = json::JSON::Object();

	subObject["playerPosX"] = ship.getPosition().x;
	subObject["playerPosX"] = ship.getPosition().y;
	subObject["lives"] = HP;
	subObject["score"] = score;
	document["Player"] = subObject;

	/*subObject

	document["LaserPos"] = subObject

	std::ofstream ostrm("./Assets/SAVES/SAVEFILE.json");
	ostrm << document.dump() << std::endl;
}*/

}

void Player::UpdateDatabase(float _totalTime)
{
	dbManager->PlayerUpdateDB(_totalTime, collCounter, deathCounter, pDestroyedEnemies, pDestroyedAsteroid);
}


