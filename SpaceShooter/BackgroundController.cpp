#include "BackgroundController.h"

BackgroundController::BackgroundController()
{
	background1 = new Background();
	background = new sf::Texture;
}

BackgroundController::~BackgroundController()
{
	delete background1;
}

void BackgroundController::Update(float _dt)
{
	background1->Update(_dt);
}

void BackgroundController::initializeJSON()
{
	std::ifstream inputStream("./Assets/JSON/BackgroundController.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON backgroundJSON = json::JSON::Load(str);

		if (!backgroundJSON.hasKey("background")) throw std::exception("background Missing");
		{
			std::string loc = backgroundJSON["background"].ToString();
			if (!background->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

}

void BackgroundController::setWindow(sf::RenderWindow* _window)
{
	window = _window;
}

void BackgroundController::setBackground()
{
	background1->SetTexture(background, window);
}
