#pragma once
#include "StandardIncludes.h"
#include "Projectile.h"
class Enemy
{
public:
	Enemy();
	virtual ~Enemy();

	virtual void SetSpawnLocation(float _x);
	virtual void SetInitMovement(bool dir, float _dt);
	virtual void Update(sf::RenderWindow* _window, float _dt);
	virtual sf::Sprite getSprite();
	virtual void ProjDraw(sf::RenderWindow* _window);
	virtual void ProjMovement(sf::RenderWindow* _window, float _dt);
	virtual int GetHP();
	virtual int GetScoreVal();
	virtual void TakeDamage(int _damage);
	virtual std::vector<Projectile>& GetProjectiles();
	virtual int getDamage();
	virtual void initializeJSON();

protected:
	sf::Sprite ship;
	sf::Texture* shipTexture = new sf::Texture;

	sf::Texture* projTexture = new sf::Texture;

	sf::Sprite initProj;
	sf::Texture* initProjTexture = new sf::Texture;


	sf::Vector2f moveDir;
	std::vector<Projectile> pLaser;
	int HP;
	int damage;
	int scoreVal;
	float shootTimer;
};

