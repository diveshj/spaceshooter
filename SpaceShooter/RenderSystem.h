#pragma once

#include <string>
#include "json.hpp"

/// <summary>
/// Render SYstem
/// </summary>
class RenderSystem
{
private:
	std::string	name;
	int			width;
	int			height;
	bool		fullscreen;

public:	
	/// <summary>
	/// Initializes a new instance of the <see cref="RenderSystem"/> class.
	/// </summary>
	RenderSystem();
	
	/// <summary>
	/// Finalizes an instance of the <see cref="RenderSystem"/> class.
	/// </summary>
	~RenderSystem();

	/// <summary>
	/// Initializes the data
	/// </summary>
	void Initialize();

	/// <summary>
	/// Lods the data
	/// </summary>
	/// <param name="_jsonNode"></param>
	void Load(json::JSON& _jsonNode);

	/// <summary>
	/// Updates the render system
	/// </summary>
	void Update();

	/// <summary>
	/// Displays the information
	/// </summary>
	void Display();
};

