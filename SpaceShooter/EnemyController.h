#pragma once
#include <vector>
#include "Enemy.h"
#include "SpecialEnemy.h"
class EnemyController
{
public:
	EnemyController();
	~EnemyController();
	void EnemyTimer(float _dt);
	void AddEnemy(float _dt);
	bool MoveDirection(float _pos);
	float GenerateSpawnLoc();
	void EnemyUpdates(float _dt);
	void EnemyDraw();
	 std::vector <Enemy>& GetEnemyT1Vec();
	 void Reset();
	 void setWindow(sf::RenderWindow* _window);

private:
	std::vector <Enemy> EnemiesT1;
	int EnemiesT1Count = 0;
	float spawnTimer = 0;
	std::vector <SpecialEnemy> spEnemies;
	int spEnemiesCount = 0;
	sf::RenderWindow* window;

};

