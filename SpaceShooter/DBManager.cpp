#include "DBManager.h"
//#include <iostream>
//#include <cassert>
//#include "sqlite3.h"


sqlite3* DBManager::Open()
{
	sqlite3* db = nullptr;

	int result = sqlite3_open("./Assets/Database/spaceshooterdb.db", &db);
	if (result)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		return nullptr;
	}

	//std::cout << "Opened database successfully" << std::endl;
	return db;

}

void DBManager::PlayerUpdateDB(float _totalTime, int _destroyedBy, int _pDeaths, int _pDestroyedEnemies, int _pDestroyedAsteroids)
{
	sqlite3* db = Open();
	assert(db != nullptr, "Unable to open database");

	std::string statement = "INSERT INTO SpaceShooterData (MatchTime, pDestroyedbyCollision, pDeathCounter, pDestroyedEnemies, pDestroyedAsteroids) VALUES (@MatchTime, @pDestroyedbyCollision, @pDeathCounter, @pDestroyedEnemies, @pDestroyedAsteroids);";


	sqlite3_stmt* stm;
	int result = sqlite3_prepare_v2(db, statement.c_str(), -1, &stm, 0);
	if (result == SQLITE_OK)
	{
		int index = sqlite3_bind_parameter_index(stm, "@MatchTime");
		sqlite3_bind_double (stm, index, _totalTime);
		int index2 = sqlite3_bind_parameter_index(stm, "@pDestroyedbyCollision");
		sqlite3_bind_int(stm, index2, _destroyedBy);
		int index3 = sqlite3_bind_parameter_index(stm, "@pDeathCounter");
		sqlite3_bind_int(stm, index3, _pDeaths);
		int index4 = sqlite3_bind_parameter_index(stm, "@pDestroyedEnemies");
		sqlite3_bind_int(stm, index4, _pDestroyedEnemies);
		int index5 = sqlite3_bind_parameter_index(stm, "@pDestroyedAsteroids");
		sqlite3_bind_int(stm, index5, _pDestroyedAsteroids);

		result = sqlite3_step(stm);
		sqlite3_finalize(stm);
		if (result != SQLITE_DONE)
		{
			std::cout << "Error, unable to INSERT" << std::endl;
		}
	}
	else
	{
		std::cout << "Failed to prepare statement: " << sqlite3_errmsg(db) << std::endl;
	}

	sqlite3_close(db);

}


 int DBManager::ReadCallback(void* data, int argc, char** argv, char** azColName)
{
	std::cout << (const char*)data << std::endl;
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " " << argv[i] << std::endl;
	}
	std::cout << "----------" << std::endl;
	return 0;
}
//
//void DBManager::Read()
//{
//	char* errMsg = nullptr;
//
//	// Open DB
//	sqlite3* db = Open();
//	assert(db != nullptr, "Unable to open database");
//
//	std::string statement = "SELECT * FROM SpaceShooterData";
//	int result = sqlite3_exec(db, statement.c_str(), ReadCallback, (void*)"READ_*", &errMsg);
//	if (result != SQLITE_OK)
//	{
//		std::cout << "Error executing read: " << errMsg << std::endl;
//		sqlite3_free(errMsg);
//	}
//
//	sqlite3_close(db);
//}


