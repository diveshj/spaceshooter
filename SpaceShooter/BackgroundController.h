#pragma once
#include "StandardIncludes.h"
#include "Background.h"
class BackgroundController
{
public:
	BackgroundController();
	~BackgroundController();

	void Update(float _dt);
	void initializeJSON();
	void setWindow(sf::RenderWindow* _window);
	void setBackground();

private:
	sf::RenderWindow* window;
	Background* background1;
	sf::Texture* background;
};

