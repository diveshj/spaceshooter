#pragma once
#include "Singleton.h"
#include "StandardIncludes.h"
#include "Player.h"
#include "EnemyController.h"
#include "SpecialEnemy.h"
#include "AsteroidController.h"
#include "Asteroid.h"
#include "BackgroundController.h"
#include "UIController.h"
#include "windowAttr.h"
#include <iostream>
#include <chrono>
class GameEngine
{
	SINGLETON(GameEngine)

public:
	void Initialize();
	void GameLoop();

private:
	windowAttr* windowattr;
	Player* player;
	EnemyController* enemyController;
	AsteroidController* asteroidController;
	BackgroundController* backgroundController;
	UIController* uiController;
};

