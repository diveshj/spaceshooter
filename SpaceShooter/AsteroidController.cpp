#include "AsteroidController.h"

AsteroidController::AsteroidController()
{
	this->timer = 0;
	this->count = 0;
}

AsteroidController::~AsteroidController()
{
	delete &asteroids;
	delete meteorBig;
	delete meteorSmall;
	delete window;
}

void AsteroidController::setWindow(sf::RenderWindow* _window)
{
	window = _window;
}

void AsteroidController::initializeJSON()
{
	std::ifstream inputStream("./Assets/JSON/AsteroidController.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON asteroidJSON = json::JSON::Load(str);

		if (!asteroidJSON.hasKey("meteorBig")) throw std::exception("meteorBig Missing");
		{
			std::string loc = asteroidJSON["meteorBig"].ToString();
			if (!this->meteorBig->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!asteroidJSON.hasKey("meteorSmall")) throw std::exception("meteorSmall Missing");
		{
			std::string loc = asteroidJSON["meteorSmall"].ToString();
			if (!this->meteorSmall->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}
	}

void AsteroidController::getCount()
{
	count = (int)asteroids.size();
}

//This function checks if there are enough asteroids in the frame, if not then its created
void AsteroidController::AsteroidTimer(float _dt)
{
	if (timer > 4 && count < 6)
	{
		int textChoice = SelectText();
		Asteroid* newAsteroid = new Asteroid();
		if (textChoice == 1)
		{
			newAsteroid->setTexture(meteorBig);
			int dir = SelectSpawnDir();
			newAsteroid->setSpawnLoc(window, dir);
			asteroids.push_back(newAsteroid);
		}
		else
		{
			newAsteroid->setTexture(meteorSmall);
			int dir = SelectSpawnDir();
			newAsteroid->setSpawnLoc(window, dir);
			asteroids.push_back(newAsteroid);
		}
		count++;
	}
	else
		timer++;
	getCount();
}

//Random selection of Texture 1 or 2.
int AsteroidController::SelectText()
{
	int text = rand() % 2 + 1;
	return text;
}

//Selects if we want to spawn them on the left or right side
int AsteroidController::SelectSpawnDir()
{
	int dir = rand() % 2 + 1;
	return dir;
}

void AsteroidController::AsteroidDraw()
{
	for (int i = 0; i < (int)asteroids.size(); i++)
	{
		window->draw(asteroids[i]->getSprite());
	}
}

//Calls update on each Asteroid and checks if they are out of bounds
void AsteroidController::AsteroidUpdate(float _dt)
{
	for (int i = 0; i < (int)asteroids.size(); i++)
	{
		asteroids[i]->Update(window, _dt);

		//Whack em if they escape the window
		if (asteroids[i]->getSprite().getPosition().y > window->getSize().y || asteroids[i]->getSprite().getPosition().y < 0 
			|| asteroids[i]->getSprite().getPosition().x > window->getSize().x || asteroids[i]->getSprite().getPosition().x < 0)
		{
			count--;
			asteroids.erase(asteroids.begin() + i);
		}
	}
}

std::vector<Asteroid*>& AsteroidController::getAsteroidsVec()
{
	return asteroids;
}

void AsteroidController::Reset()
{
	count = 0;
	asteroids.clear();
}
