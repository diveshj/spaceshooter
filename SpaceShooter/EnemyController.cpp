#include "EnemyController.h"
#include <random>

EnemyController::EnemyController()
{
}

EnemyController::~EnemyController()
{
}

//Has a timer to how often the enemy should spawn, its set to default by 2 seconds
void EnemyController::EnemyTimer(float _dt)
{
	spawnTimer += _dt;

	if (spawnTimer > 2)
	{
		AddEnemy(_dt);
		spawnTimer = 0;
	}
}

//Spawns an enemy based on windowsize and the direction
void EnemyController::AddEnemy(float _dt)
{
	float spawnPos = GenerateSpawnLoc();
	bool dir = MoveDirection(spawnPos);

		Enemy* temp = new Enemy();
		temp->SetSpawnLocation(spawnPos);
		temp->SetInitMovement(dir, _dt);
		EnemiesT1.push_back(*temp);
		EnemiesT1Count++;
}

//Direction they need to move, false is left, true is right
bool EnemyController::MoveDirection(float _pos)
{
	bool dir = false;
	if (_pos > window->getSize().x * 0.5)
	{
		dir = false;
	}
	else { dir = true; }

	return dir;
}

//Generates a random spawn position along the X axis of the window.
float EnemyController::GenerateSpawnLoc()
{
	int xPos = window->getSize().x;

	const int x_MIN = 0;
	const int x_MAX = xPos;

	std::random_device rd;
	std::default_random_engine eng(rd());
	std::uniform_int_distribution<> distrx(x_MIN, x_MAX);
	xPos = distrx(eng);

	return xPos;
}

void EnemyController::EnemyUpdates(float _dt)
{
		for (int i = 0; i < (int)EnemiesT1.size(); i++)
		{
			this->EnemiesT1[i].Update(window, _dt);

			if (EnemiesT1[i].getSprite().getPosition().y > window->getSize().y)
			{
				EnemiesT1Count--;
				EnemiesT1.erase(EnemiesT1.begin() + i);
			}
		}
}

void EnemyController::EnemyDraw()
{
	for (int i = 0; i < (int)EnemiesT1.size(); i++)
	{
		window->draw(EnemiesT1[i].getSprite());
	}
}

std::vector<Enemy>& EnemyController::GetEnemyT1Vec()
{
	return EnemiesT1;
}

void EnemyController::Reset()
{
	EnemiesT1.clear();
	spEnemies.clear();
	EnemiesT1Count = 0;
}

void EnemyController::setWindow(sf::RenderWindow* _window)
{
	window = _window;
}
