#pragma once
#include "Enemy.h"
class SpecialEnemy :    public Enemy
{
public:
	SpecialEnemy();
	~SpecialEnemy();
	void SetSpawnLocation(float _x);
	void SetInitMovement(bool dir, float _dt);
	void Update(sf::RenderWindow* _window, float _dt);
	 sf::Sprite getSprite();
	 void ProjDraw(sf::RenderWindow* _window);
	void ProjMovement(sf::RenderWindow* _window, float _dt);
	int GetHP();
	int GetScoreVal();
	void TakeDamage();
};

