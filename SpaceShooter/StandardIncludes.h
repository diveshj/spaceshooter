#ifndef _STANDARDINCLUDES_H
#define _STANDARDINCLUDES_H

//SFML headers
#include "SFML/Graphics.hpp"
#include "SFML/System.hpp"
#include "json.hpp"
#include <fstream>

//Getting tired of writing SFML headers in each .h file? Declare a header so you have to type one line instead of two.

#endif // ! _STANDARDINCLUDES_H
