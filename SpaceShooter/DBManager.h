#pragma once
#include <iostream>
#include <cassert>
#include "sqlite3.h"
#include <string>
class DBManager
{
public:
	sqlite3* Open();
	void PlayerUpdateDB(float _totalTime, int _destroyedBy, int _pDeaths, int _pDestroyedEnemies, int _pDestroyedAsteroids);
	static int ReadCallback(void* data, int argc, char** argv, char** azColName);
	//void Read();
};