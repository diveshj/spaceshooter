#pragma once
#include "StandardIncludes.h"
class Background
{
public:
	Background();
	~Background();
	void SetTexture(sf::Texture* _texture, sf::RenderWindow* _window);
	void Update(float _dt);
private:
	sf::Sprite backgroundSprite;
	sf::RenderWindow* window;

};

