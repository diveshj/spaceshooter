#include "Background.h"

Background::Background()
{	
}

Background::~Background()
{
}

void Background::SetTexture(sf::Texture* _texture, sf::RenderWindow* _window)
{
	this->window = _window;
	_texture->setRepeated(true);
	backgroundSprite.setTexture(*_texture);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, window->getSize().x, window->getSize().y * 3));
	backgroundSprite.setOrigin(0.0f, backgroundSprite.getTextureRect().height * 0.25);
}

void Background::Update(float _dt)
{
	backgroundSprite.move(0.0f, 25.0f * _dt);
	if (backgroundSprite.getPosition().y > window->getSize().y * 0.70)
	{
		backgroundSprite.setPosition(0, 0);
	}
	window->draw(backgroundSprite);
}
