#include "Asteroid.h"
#include <iostream>
Asteroid::Asteroid()
{

}

Asteroid::~Asteroid()
{
}

//Sets the texture passed to it
void Asteroid::setTexture(sf::Texture* _texture)
{
	this->asteroid.setTexture(*_texture);
}

//Sets the spawn location of the asteroid to either direction and sets the movement vector towards +ve or -ve axis
void Asteroid::setSpawnLoc(sf::RenderWindow* _window, int dir)
{
	if (dir == 1)
	{

		float randPos = rand() % _window->getSize().y + 0;
		this->asteroid.setPosition(0.0, randPos);
		
		float randDir = rand() % 70 + 10;

		this->setMoveDir(sf::Vector2f(70.0f, randDir));
	}
	else 
	{
		float randPos = rand() % _window->getSize().y + 0;
		this->asteroid.setPosition(_window->getSize().x, randPos);
		float randDir = rand() % 70 - 10;
		this->setMoveDir(sf::Vector2f(-70.0f, randDir));
	}
	
}
//Sets the movement vector passed so it continues in that direction
void Asteroid::setMoveDir(sf::Vector2f _dir)
{
	moveDir.x = _dir.x;
	moveDir.y = _dir.y;
}

void Asteroid::Update(sf::RenderWindow* _window, float _dt)
{
	this->asteroid.move(moveDir.x * _dt, moveDir.y * _dt);
}

//Accessor
sf::Sprite Asteroid::getSprite()
{
	return asteroid;
}

int Asteroid::getScoreVal()
{
	return points;
}

