#include "windowAttr.h"
#include <fstream>
#include "json.hpp"

void windowAttr::getWindowAttrJSON()
{
	std::ifstream inputStream("./Assets/JSON/windowAttr.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON windowAttrJSON = json::JSON::Load(str);

		if (!windowAttrJSON.hasKey("width")) throw std::exception("width Missing");
		width = windowAttrJSON["width"].ToInt();

		if (!windowAttrJSON.hasKey("height")) throw std::exception("height Missing");
		height = windowAttrJSON["height"].ToInt();

		if (!windowAttrJSON.hasKey("title")) throw std::exception("title Missing");
		title = windowAttrJSON["title"].ToString();
}