#include "Enemy.h"
#include <iostream>

Enemy::Enemy()
{
	initializeJSON();
	this->shootTimer = 0;
	this->moveDir.x = 0;
	this->moveDir.y = 0;

}

Enemy::~Enemy()
{
}

//Sets the spawn location on the X axis somewhere
void Enemy::SetSpawnLocation(float _x)
{
	this->ship.setPosition(_x, 0.0f);
}

//Sets the initial direction movement based on the bool provided. Moves left if spawned on right, right if spawned on left
void Enemy::SetInitMovement(bool dir, float _dt)
{
	if (dir==false)
	{
		moveDir.x = -100.0f;
		moveDir.y = 120.0f;
		this->ship.move(moveDir.x * _dt, moveDir.y * _dt);
	}
	else
	{
		moveDir.x = 100.0f;
		moveDir.y = 120.0f;
		this->ship.move(moveDir.x * _dt, moveDir.y * _dt);
	}
}

//All updates on Enemy, shooting, general updates
void Enemy::Update(sf::RenderWindow* _window, float _dt)
{
	if (shootTimer > 24)
	{
		sf::Vector2f laserVec = ship.getPosition();
		laserVec.x = laserVec.x + (shipTexture->getSize().x * 0.5);
		laserVec.y = laserVec.y + (shipTexture->getSize().y);
		this->pLaser.push_back(Projectile(projTexture, laserVec));

		sf::Vector2f initProjVec = ship.getPosition();
		initProjVec.x = initProjVec.x + (shipTexture->getSize().x * 0.5);
		initProjVec.y = initProjVec.y + (shipTexture->getSize().y);
		Projectile* temp = new Projectile(initProjTexture, initProjVec);
		_window->draw(temp->projectile);

		shootTimer = 0;
	}
	shootTimer++;
	ProjDraw(_window);
	ProjMovement(_window, _dt);

	this->ship.move(moveDir.x * _dt, moveDir.y * _dt);

}

sf::Sprite Enemy::getSprite()
{
	return ship;
}

//Draws the laser
void Enemy::ProjDraw(sf::RenderWindow* _window)
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		_window->draw(pLaser[i].projectile);
	}
}


//laser flowing through the world and its deletion
void Enemy::ProjMovement(sf::RenderWindow* _window, float _dt)
{
	for (int i = 0; i < (int)pLaser.size(); i++)
	{
		this->pLaser[i].projectile.move(0.0f, 350.0f * _dt);

		if (pLaser[i].projectile.getPosition().y > _window->getSize().y)
		{
			this->pLaser.erase(pLaser.begin() + i);
		}
	}
}

int Enemy::GetHP()
{
   	return HP;
}

int Enemy::GetScoreVal()
{
return scoreVal;
}

void Enemy::TakeDamage(int _damage)
{
	this->HP -= _damage;
}

std::vector<Projectile>& Enemy::GetProjectiles()
{
	return pLaser;
}

int Enemy::getDamage()
{
	return damage;
}

void Enemy::initializeJSON()
{
	std::ifstream inputStream("./Assets/JSON/Enemy.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON EnemyFileJSON = json::JSON::Load(str);

		if (!EnemyFileJSON.hasKey("HP")) throw std::exception("HP Missing");
		this->HP = EnemyFileJSON["HP"].ToInt();

		if (!EnemyFileJSON.hasKey("shipTexture")) throw std::exception("shipTexture Missing");
		{
			std::string loc = EnemyFileJSON["shipTexture"].ToString();
			if (!this->shipTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!EnemyFileJSON.hasKey("initProjTexture")) throw std::exception("initProjTexture Missing");
		{
			std::string loc = EnemyFileJSON["initProjTexture"].ToString();
			if (!this->initProjTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!EnemyFileJSON.hasKey("projTexture")) throw std::exception("projTexture Missing");
		{
			std::string loc = EnemyFileJSON["projTexture"].ToString();
			if (!this->projTexture->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}
		if (!EnemyFileJSON.hasKey("damage")) throw std::exception("damage Missing");
		this->damage = EnemyFileJSON["damage"].ToInt();

		if (!EnemyFileJSON.hasKey("scoreVal")) throw std::exception("scoreVal Missing");
		this->scoreVal = EnemyFileJSON["scoreVal"].ToInt();
	this->ship.setTexture(*shipTexture);

	this->initProj.setTexture(*initProjTexture);

}

