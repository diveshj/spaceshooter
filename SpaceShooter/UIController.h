#pragma once
#include "StandardIncludes.h"
class UIController
{
public:
	UIController();
	~UIController();
	void UIsetScore(int _score);
	void UIsetLives(int _lives);
	void DisplayLives();
	void DisplayText();
	void initializeJSON();
	void DisplayGameOver();
	void setWindow(sf::RenderWindow* _window);
	void initalizeText();
	void writeHS();

private:
	sf::RenderWindow* window;
	
	int highscore = 0;
	int score = 0;
	sf::Font* font = new sf::Font();
	sf::Text* scoreText = new sf::Text();
	sf::Text* HighScoreText= new sf::Text();
	sf::Text* gameOverText = new sf::Text();
	
	sf::Text* livesText = new sf::Text();
	int lives = 0;
	sf::Texture* textLives = new sf::Texture();
	sf::Sprite pLives;
	std::vector<sf::Sprite> dispLives;
};

