#include "UIController.h"

UIController::UIController()
{
}

UIController::~UIController()
{
}

void UIController::UIsetScore(int _score)
{
	score = _score;
}

void UIController::UIsetLives(int _lives)
{

	lives = _lives;
}

void UIController::DisplayLives()
{
	dispLives.clear();

	for (int i = 1; i <= lives; i++)
	{
		dispLives.push_back(pLives);
	}
	sf::Vector2f temp(window->getSize().x * 0.48, window->getSize().y * 0.1);
	for (int i = 0; i < (int)dispLives.size(); i++)
	{
		dispLives[i].setOrigin(dispLives[i].getLocalBounds().width * 0.5, dispLives[i].getLocalBounds().height * 0.5);
		dispLives[i].setPosition(temp);
		temp.x += 30;
		window->draw(dispLives[i]);
	}
}

void UIController::DisplayText()
{
	scoreText->setString("Score : " + std::to_string(score));
	window->draw(*scoreText);


	if (score > highscore)
	{
		highscore = score;
	HighScoreText->setString("HighScore : " + std::to_string(score));
	}

	else
	HighScoreText->setString("HighScore : " + std::to_string(highscore));

	window->draw(*HighScoreText);

	livesText->setString("Lives");
	window->draw(*livesText);
}

//Load up all the data from JSON
void UIController::initializeJSON()
{
	std::ifstream inputStream("./Assets/JSON/UIController.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON UIControllerJSON = json::JSON::Load(str);

		if (!UIControllerJSON.hasKey("textLives")) throw std::exception("textLives Missing");
		{
			std::string loc = UIControllerJSON["textLives"].ToString();
			if (!textLives->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}

		if (!UIControllerJSON.hasKey("font")) throw std::exception("font Missing");
		{
			std::string loc = UIControllerJSON["font"].ToString();
			if (!font->loadFromFile(loc))
			{
				std::cout << "Unable to load " + loc << std::endl;
			}
		}
		if (!UIControllerJSON.hasKey("Highscore")) throw std::exception("Highscore Missing");
		highscore = UIControllerJSON["Highscore"].ToInt();
	pLives.setTexture(*textLives);
}

void UIController::DisplayGameOver()
{
	gameOverText->setFont(*font);
	gameOverText->setCharacterSize(70);
	gameOverText->setFillColor(sf::Color::Red);
	gameOverText->setOrigin(gameOverText->getLocalBounds().width * 0.5, gameOverText->getLocalBounds().height * 0.5);
	gameOverText->setPosition(window->getSize().x * 0.50, window->getSize().y * 0.50);
	gameOverText->setString("Game Over. Press R to restart");
	window->draw(*gameOverText);
}

void UIController::setWindow(sf::RenderWindow* _window)
{
	window = _window;
}

void UIController::initalizeText()
{
	scoreText->setFont(*font);
	scoreText->setCharacterSize(40);
	scoreText->setFillColor(sf::Color::White);
	scoreText->setPosition(window->getSize().x * 0.15, 0);

	HighScoreText->setFont(*font);
	HighScoreText->setCharacterSize(40);
	HighScoreText->setFillColor(sf::Color::White);
	HighScoreText->setPosition(window->getSize().x * 0.70, 0);

	livesText->setFont(*font);
	livesText->setCharacterSize(40);
	livesText->setFillColor(sf::Color::White);
	livesText->setPosition(window->getSize().x * 0.48, 0);
}

void UIController::writeHS()
{
	int HSTemp = 0;
	std::string textLiveLoc;
	std::string fontLoc;

	std::ifstream inputStream("./Assets/JSON/UIController.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON UIControllerJSON = json::JSON::Load(str);

		if (!UIControllerJSON.hasKey("textLives")) throw std::exception("textLives Missing");
		{
			textLiveLoc = UIControllerJSON["textLives"].ToString();
		}

		if (!UIControllerJSON.hasKey("font")) throw std::exception("font Missing");
		{
			fontLoc = UIControllerJSON["font"].ToString();
		}
		if (!UIControllerJSON.hasKey("Highscore")) throw std::exception("Highscore Missing");
		HSTemp = UIControllerJSON["Highscore"].ToInt();

	//Replaces JSON file with the newer one
	if (HSTemp < highscore)
	{
		json::JSON document;
		document["textLives"] = textLiveLoc;
		document["font"] = fontLoc;
		document["Highscore"] = highscore;
		std::ofstream ostrm("./Assets/JSON/UIController.json");
		ostrm << document.dump() << std::endl;
	}
}
