#include "Projectile.h"

Projectile::Projectile(sf::Texture* texture, sf::Vector2f pos)
{
	this->projectile.setTexture(*texture);
	this->projectile.setOrigin(texture->getSize().x * 0.5, texture->getSize().y * 0.5);
	this->projectile.setPosition(pos);
}

Projectile::~Projectile()
{
}

sf::Sprite Projectile::GetSprite()
{
	return projectile;
}
